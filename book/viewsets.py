from rest_framework import viewsets

from .models import Book
from .serializer import BookSerializer
"""Esta clase nos permite hacer el CRUD sobre los objetos de nuestro modelo"""
class BookViewSet(viewsets.ModelViewSet):
    queryset = Book.objects.all()
    serializer_class = BookSerializer