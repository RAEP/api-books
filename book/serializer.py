from .models import Book
from rest_framework import serializers
'''Creación de la clase serializer para mandar los datos'''
class BookSerializer(serializers.ModelSerializer):
    '''La clase Meta permite definar la información que se ocupara'''
    class Meta:
        """Definimos el modelo que vamos a utilizar"""
        model = Book 
        fields = '__all__' 
        """Definimos los campos que vamos a mostrar"""