from rest_framework import routers

from .viewsets import BookViewSet

"""Define las rutas basicas de nuestro modelo (GET,PUT,POST,DELETE)"""
router = routers.SimpleRouter()
"""Registrar nuestra ruta"""
router.register('books',BookViewSet)

urlpatterns = router.urls